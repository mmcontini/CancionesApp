﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CancionesApp
{
    class CancionBean
    {
        private String titulo;
        private int anio;

        public String Titulo
        {
            get { return titulo; }
            set { titulo = value; }
        }

        public int Anio
        {
            get { return anio; }
            set { anio = value; }
        }

        public String getInfo() {
            return "Título: " + Titulo + " - Año: " + anio;
        }    
    }
}
