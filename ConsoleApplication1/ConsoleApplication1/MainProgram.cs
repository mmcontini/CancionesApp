﻿using CancionesApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

class MainProgram
{

    static List<CancionBean> listaCanciones = new List<CancionBean>();

    static void Main(string[] args)
    {
        Console.WriteLine(" ********************************************************** ");
        Console.WriteLine(" Ingrese una de las siguientes opciones: ");
        Console.WriteLine(" 1: Cargar canción ");
        Console.WriteLine(" 2: Mostrar lista de canciones ");
        Console.WriteLine(" 3: Salir ");
        Console.WriteLine(" ********************************************************** ");
        String opcionElegida = Console.ReadLine();
        procesarOpciones(opcionElegida);
    }

    static void procesarOpciones(String opcion)
    {
        String opcionvolver = null;
        switch (opcion) {
            case "1":
                CancionBean cb = new CancionBean();
                Console.Write(" Título: ");
                String titulo = Console.ReadLine();
                if (!String.IsNullOrEmpty(titulo)) {
                    cb.Titulo = titulo;
                } else {
                    Console.WriteLine(" Error: Debe ingresar algún valor ");
                    Console.WriteLine(" Presione cualquier tecla para volver al menú ..... ");
                    opcionvolver = Console.ReadLine();
                    Console.Clear();
                    Main(new String[0]);
                }

                if (!String.IsNullOrEmpty(titulo)) {
                    Console.Write(" Año: ");
                    String anio = Console.ReadLine();
                    int j;
                    bool resultadoOk = Int32.TryParse(anio, out j);
                    if (resultadoOk)
                    {
                        if(Regex.IsMatch(anio, "^(19|20)[0-9][0-9]") && anio.Length == 4) {
                            cb.Anio = j;
                        } else {
                            Console.WriteLine(" Error: El año ha sido ingresado correctamente ");
                            Console.WriteLine(" Presione cualquier tecla para volver al menú ..... ");
                            opcionvolver = Console.ReadLine();
                            Console.Clear();
                            Main(new String[0]);
                        }
                    }
                    else
                    {
                        Console.WriteLine(" Error: El número que ha ingresado no es numérico ");
                        Console.WriteLine(" Presione cualquier tecla para volver al menú ..... ");
                        opcionvolver = Console.ReadLine();
                        Console.Clear();
                        Main(new String[0]);
                    }
                    listaCanciones.Add(cb);
                    Console.Clear();
                    Main(new String[0]);
                }
                else
                {
                    Console.WriteLine(" Error: Debe ingresar algún valor ");
                    Console.WriteLine(" Presione cualquier tecla para volver al menú ..... ");
                    opcionvolver = Console.ReadLine();
                    Console.Clear();
                    Main(new String[0]);
                }
                break;
            case "2":
                Console.Clear();
                if (listaCanciones.Any())
                {
                    Console.WriteLine(" ********************* Lista de canciones ********************* ");
                    foreach (var cancion in listaCanciones)
                    {
                        Console.WriteLine(cancion.getInfo());
                    }
                    Console.WriteLine(" *************************************************************** ");
                } else {
                    Console.WriteLine(" ********************* No hay datos para mostrar ********************* ");
                }
                Console.WriteLine(" Presione cualquier tecla para volver al menú ..... ");
                opcionvolver = Console.ReadLine();
                Console.Clear();
                Main(new String[0]);
                break;
            case "3":
                Console.WriteLine(" ********************* Fin de la ejecución ********************* ");
                break;
            default:
                break;
        }

    } 
}